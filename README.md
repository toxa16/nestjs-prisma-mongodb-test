# NestJS + Prisma + MongoDb Test Task

To complete the test, you will be asked to create a [NestJS](https://nestjs.com/) application, which connects to a MongoDb database using [Prisma ORM](https://www.prisma.io/). Usage of other tools and libraries are under your consideration.

After completing the task, you will be asked to **publish your code to your public repository**.

The task is split into 8 stages. Complete as many of them as you can. It is recommended to complete the stages in the order they are appearing in the list (except the stage 8).

## Data Model

![](./data-model.png)

## Stages

1. Implement REST API for each of the entities shown on the data model above. The API of each entity must include following endpoints:

   - `GET /<ENTITY>` - fetch all;
   - `GET /<ENTITY>/:id` - fetch one;
   - `POST /<ENTITY>` - create one;
   - `PATCH /<ENTITY>/:id` - update one;
   - `DELETE /<ENTITY>/:id` - delete one (ignoring relations).

2. Add [validation](https://docs.nestjs.com/techniques/validation) to the endpoints (request body, URL params).

3. [Document](https://docs.nestjs.com/openapi/introduction) the API with Swagger.

4. Implement data seeding: create test data (fixtures) for the entities and implement the following endpoints:

   - `POST /seed` - clear the database and fill it with the fixtures;
   - `DELETE /seed` - clear the database.

5. [Configure](https://docs.nestjs.com/techniques/configuration) development and production environments (via `NODE_ENV` environment variable): 

   - `NODE_ENV = "development"` - development environment;
   - `NODE_ENV = "production"` - production environment.

   Disable data seeding from stage 4 (if implemented) for production environment.

6. Implement batch endpoints for all entities (as many as you can):

   - `POST /<ENTITY>/batch` - create many;
   - `PATCH /<ENTITY>/batch` - update many;
   - `DELETE /<ENTITY>/batch` - delete many (ignoring relations).

7. Implement advanced deletion scheme (choose one):

   - _cascade_ - e.g. when a `User` object is deleted, all `Post`s and `Rating`s that reference the `User` must be deleted as well;
   - _restrict_ - e.g. a `User` can't be deleted if there are referencing `Post` or `Rating` objects.

8. Cover your code with tests (if you are working in the TDD way, you can start completing this stage while working on other stages).

Good luck and happy hacking!